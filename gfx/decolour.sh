for f in *.eps; do ps2pdf -dEPSCrop -dCompressPages=false $f ${f%.eps}.pdf; done
for f in *.pdf; do sed "/^0 g$/d" $f -i; done
